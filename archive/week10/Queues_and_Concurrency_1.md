# Queues

A queue is data structure that maintains an ordered collection of items. The queue typically supports just two
operations:

  * Enqueue (aka "put")"- add a new item to the queue.
  * Dequeue (aka "get")- remove an item from the queue.

Items are removed from a queue in First-In-First-Out (FIFO) fashion: that is, the item removed from the first dequeue
operation will be the first item added to the queue, the item removed from the second dequeue operation will be the
second item added to the queue, and so on.

Sometimes queues are referred to as "FIFO Queues" for emphasis.


## Basic Example

Consider the set of (abstract) operations on a Queue object.

```
  1. Enqueue 5
  2. Enqueue 7
  3. Enqueue A
  4. Dequeue
  5. Enqueue 1
  6. Enqueue 4
  7. Dequeue
  8. Dequeue
```

The order of items returned is:

```
5, 7, A
```

And the contents of the Queue after Step 8 is

```
1, 4
```

## In-memory Python Queues

The Python standard library provides an in-memory Queue data structure via its `queue` module. To get started, import the
`queue` module and instantiate a `queue.Queue` object:

```
>>> import queue
>>> q = queue.Queue()
```

The Python Queue object has the following features:

  * The `q` object supports `.put()` and `.get()` to put a new item on the queue, and get an item off the queue, respectively.
  * `q.put()` can take an arbitrary Python object and `q.get()` returns that object.

Let's perform the operations above using the `q` object.

```
Exercise. Use a series of q.put() and q.get() calls to perform Steps 1-8 above. Verify the the order of items returned.

Exercise. Verify that arbitry Python objects can by put onto and retrieved from the queue by inserting a list and a
dictionary.

```

Queues are a fundamental ingredient in concurrent programming, a topic we will turn to next.


# Concurrency

A computer system is said to be concurrent if multiple agents or components of the system can be in progress at the
same time.

While components of the system are in progress at the same time, the individual operations themselves may happen
sequentially. In general, a system being concurrent means that the different components can be executed at the same time
or in different orders without impacting the overall correctness of the system.


## A First Example

Suppose we want to build a system for maintaining the balance of a bank account where multiple agents are acting on the account
(withdrawing and/or depositing funds) at the same time. Let's consider two different approaches.

Approach 1. Whenever an agent receives an order to make a deposit or withdraw, the agent

  * A. Makes a query to determine te current balance.
  * B. Computes the new balance based on the deposit or withdraw amount.
  * C. Makes a query to update the balance to the computed amount.

This approach is not concurrent because the individual operations of different agents cannot be reordered. For example,
suppose we have two agents, agent 1 and 2, and a starting balance of $50.

  * Agent 1 gets an order to deposit $25 at the same time that agent 2 gets an order to withdraw $10.
  * The final balance should be $65, and the system will arrive at this answer as long as all three steps A, B and C
  for one agent are done before any steps for the other agent are started; for ex, 1A, 1B, 1C, 2A, 2B, 2C.
  * However, if the steps of the two agents are mixed - for instance: 1A, 2A, 1B, 2B, 1C, 2C - then the system will
   not arrive at the correct answer. In this case, the system will compute the final balance to be $40!

Approach 2. Whenever an agent receives an order to make a withdraw or deposit, the agent simply writes the
order to a queue; a positive number indicates a deposit while a negative number indicates a withdraw. The account
system keeps a running "balancer" agent whose only job is to read items off the queue and update the balance.

This approach is concurrent because the order of the agents' steps can be mixed without impacting the overall result.
This fact essentially comes down to the commutativity of addition and subtraction operations: i.e., `50 + 25 - 10 = 50 - 10 + 25 `.

Note that the queue of orders could be generalized to a "queue of tasks" (transfer some amount from account A to account B,
close account C, etc.).


## Threads

There are many different models and approaches to concurrent programming. One popular model available in many modern
programming languages and operating systems is threads. The precise
mechanics of threads varies by operating system and programming language, but generally speaking:

  * Threads provide concurrency within a single OS process, e.g., a single Python program.
  * All threads within the same process share memory; in particular, global objects are available in all threads.
  * In Python, threads are created and managed via the `threading` library.

While threads are an important and fundamental topic in concurrent programming, we will likely not have a use for
them in this class. The fact that threads are limited to a single operating system process is too restrictive for our purposes.


## Queues in Redis

The Python in-memory queues are very useful for a single Python program, but we ultimately want to share queues across
multiple Pythpn programs/VMs.

The Redis DB can be used to provide a queue data structure for clients running in different OS processes across multiple
computers. The basic idea is:

  * Use a Redis list data structure to hold the items in the queue.
  * Use the Redis list operations `rpush`, `lpop`, `llen`, etc. to create a queue data structure.

For example:

  * `rpush` will add an element to the end of the list.
  * `lpop` will return an element from the front of the list, and return nothing if the list is empty.
  * `llen` will return the number of elements in the list.


Fortunately, we don't have to implement the queue ourselves, but know that if we needed to we could without too much effort.

### Using the hotqueue Library

We will leverage a small, open source Python library called `hotqueue` which has already implemented the a Queue
data stucture in Redis using the approach outlined above. Besides not having to write it ourselves, the use of `hotqueue`
will afford us a few additional features which we will look at later.

Here are the basics of the `hotqueue` library:

  * Hotqueue is not part of the Python standard library; you can install it with `pip install hotqueue`
  * Creating a new queue data structure or connecting to an existing queue data structure is accomplished by creating a `HotQueue` object.
  * Constructing a `HotQueue` object takes very similar parameters to that of te `StrictRedis` but also takes a `name` attribute. The `HotQueue` object ultimately provides a connection to the Redis server.
  * Once constructed, a `HotQueue` object has `.put()` and `.get()` methods that act just like the corresponding
    methods of an in-memory Python queue.

### An Example

First, start up the Redis database container and then start up a container based on our `redis-client` image:

```
# start up redis..
$ docker run -p 6379:6379 -d redis:5.0.0

# start up a client container in bash
$ docker run -it --rm --entrypoint=bash jstubbs/redis-client
```

Once attached to the redis client container, first install the `hotqueue` library:

```
$ pip install hotqueue
```

Then start an iPython session and create a `hotqueue.Queue` object:

```
>>> from hotqueue import HotQueue
>>> q = HotQueue("queue", host="172.17.0.1", port=6379, db=0)
```

Note how similar the `HotQueue()` instantiation is to the `StrictRedis` instantiation. In the example above we named the queue
"queue" -- pretty creative, don't you think?

Now we can add elements to the queue using the `.put()`; just like with in-memory Python queues, we can put any Python
object into the queue:

```
>>> q.put(1)
>>> q.put('abc')
>>> q.put(['1', 2, {'key': 'value'}, '4'])
```

We can check the number of items in queue at any time using the `len` built in:

```
>>> len(q)
3
```

And we can remove an item with the `.get()` method; remember - the queue follows a FIFO principle:

```
>>> q.get()
1
>>> len(q)
2
>>> q.get()
'abc'
>>> len(q)
1
```

Under the hood, the `hotqueue.Queue` is just a Redis object, which we can verify using a redis client:

```
>>> import redis
>>> rd = redis.StrictRedis(host="172.17.0.1", port=6379, db=0)
>>> rd.keys()
[b'hotqueue:queue']
```
Note that the queue is just a single key in the Redis server `(db=0)`.

And just like with other Redis data structures, we can connect to our queue from additional Python clients and see
the same data.

```
Exercise. In a second SSH shell, start up another redis-client container, install hotqueue, start iPython and connect
 to the same queue. Prove that you can use get and put to "communicate" between your two Python programs.
```




