# Cloud Computing

## What is a cloud?

A "cloud" is a computer system that provides users with shared access to on-demand computing resources via the internet.

![picture](https://imgs.xkcd.com/comics/the_cloud.png) 

### Goals/Benefits
* Resource utilization
* Scalability and Elasticity 
* Reproducibility by way of Programmability
* Reliability through redundancy
* Geographically distributed

This is what "Devops" is all about.

### Charge Model

* Cpu 
* Memory
* Electricity/Cooling
* Bandwidth 
    * Egress/Ingress
* Storage 
    * Egress/Ingress

## Cloud Means Different Things to Different Folks

Examples of different kinds of cloud users:

* System Administrators - use cloud to automate operations.
* Software Developers - build applications on cloud servers and platforms.
* Computational scientists - Write codes to analysis scientific data in the cloud.

## Cloud service models

- Cloud vs HPC
- VMs vs Containers
- generally speaking, Containers vs APIs

Clouds can offer different service models:

* Infrastructure-as-a-service (Iaas) - virtual servers, networks, firewalls, etc. (Openstack, Jetstream, AWS/EC2, Azure)
* Platform-as-a-service (Paas) - deploy applications without managing virtual servers (Google App Engine, Heroku)
* Software-as-a-service (Saas) - Ready to use software application (Gmail, Office365, Bitbucket)
* Also: Emerging models such as functions-as-a-service. (Abaco, AWS Lambda, Google Cloud Functions)

![picture](clouddental.jpg)


## Infrastrcture-as-a-service

### Virtual Machines (VMs) / Instances

* Virtual Machines (VMs) - Simulate a physical computer through software.
* crops vs houseplant
* Flavors: (m1.tiny = 1cpu/2gb, m1.small = 2cpu/4gb, m1.medium = 4cpu/8gb, etc.) 
* Tools to manage: Ansible, Puppet, Chef, Salt, etc.

### Storage 

* Object vs Block storage

#### Block storage (volumes) 

* Like a USB drive for your virtual machine
* Can be detached and plugged in to any VM, but only one at a time
* Like a blank hard drive, must be formatted
* Standard POSIX filesystem interface (ls, cd, cat, etc.)

#### Object storage (s3/swift) 

* Accessed by API (or curl)
* 2 kinds of objects: buckets and files 
* Every file is a url. e.g. https://www.dropbox.com/s/dh00ija2sk4mswq/hunqapillar.jpg?dl=0
* May require more effort to make use of in a program
* Often used for more scaleable apps because the file is not necessarily present on the web server
* Performance implications due to network transfer time

### Networking

* Software defined networking: Routers, networks and subnets - used to connect VMs to other computers.
* Private: non-routable ips (not accessible from public internet) (https://en.wikipedia.org/wiki/Private_network)
* Public: routable to public internet
* Note that Docker creates its own private network on top of your vm's private network (which is already virtual in Openstack)

![picture](https://s3.amazonaws.com/cdn.kinsmangarden.com/images/masonbeenests450.jpg)

### Security 

* Firewall
* Security groups - firewall rules enabling or disabling network traffic to/from ports on the VMs.
* Encryption
* SSH Keys
* SSL/TLS Encryption 
* Multi-Factor Authentication
* Authentication vs. Authorization
  
## Openstack

"OpenStack is a cloud operating system that controls large pools of compute, storage, and networking resources throughout a datacenter, all managed through a dashboard that gives administrators control while empowering their users to provision resources through a web interface."

* https://www.openstack.org
* A huge Open Source collection of projects. 
* APIs mostly written in Python.
* Leverages many existing Linux technologies such as virtualization, network bridges, logical volume management, etc.
* Jetstream: A national science and engineering cloud built on Openstack.

  
## Hands on: Launching a VM on the JetStream cloud

1. Navigate to the JetStream Horizon interface: https://tacc.jetstream-cloud.org
2. Login with your XSEDE User Name and Password and enter "tacc" for the Domain.
3. Make sure you see our class project id (TG-CCR180044) in the projects drop down at the top and select it if it is not already selected (check mark):

![picture](jetstream_horizon_projects.png)

4. Select Access & Security -> Key Pairs -> Create Key Pair. Give your key a name and click "Create Key Pair". When prompted, download the key file to your local desktop or click the Download link.
5. Instances -> Launch Instance.
    * Details: Name your instance something like your_username-2 and click Next (Don't click launch instance)
    * **Source: Click the "No" under "Create New Volume"** 
    * Source: Enter JS-API-Featured-Ubuntu18-Sep-18-2018 in the search and click the plus (+) to select that image.
    * Flavor: Click the plus next to m1.small
    * Networks: Choose the TG-CCR180044-coe332-subnet by clicking plus.  * (Skip Network Ports by clicking next)
    * Security Groups: Select default security group.
    * Key Pair: Select the key you just created in step 4.
    * Click Launch instance.
6. Once the VM has been spawned, click the arrow at the far right to select "Associate Floating IP" from the available instance actions.

![picture](jetstream_horizon_instance_actions.png)

7. Select an IP in the drop down (if there is not one, click the plus (+) symbol to allocate one) and click Associate.


### Verify connectivity to the VM

Use the SSH key and IP address you generated in steps 4 and 7 above, respectively to test connectivity to your new VM.

OSX/Linux:

```
 Open the application 'Terminal'
  # make sure your key file has the correct permissions:
  $ chmod 0600 chmod 0600 ~/Downloads/<key_name>.pem
  # connect to the VM over SSH
  $ ssh -i ~/Downloads/<key_name>.pem ubuntu@<IP address>
```

Windows:

```
 Open the application 'PuTTY'
  enter Host Name: <IP address>
  (click 'Open')
  (enter "ubuntu" username)
  (select your key)
```

If all goes well you should see a command prompt that looks something like:

```
Last login: Sat Jul 22 17:52:45 2017 from dhcp-146-6-176-22.tacc.utexas.edu
ubuntu@mpackard2:~$ 
```

Add the ubuntu user to the docker group so you can run docker commands without sudo:

```
$ sudo usermod -a -G docker ubuntu
```

### Add a storage volume to the VM

* Click Volumes
* Click Create Volume
* Volume name: your_name_vol
* Size: 10 GB
* After it's created, click on the Actions menu -> Manage attachments
* Choose your VM name
* If it works, it should say something like 
     
```
/dev/sdb on mpackard-1
```
* Login to your vm, run 
```
$ sudo cat /proc/partitions 
... 
8       16   10485760 sdb

$ sudo mkdir /data

$ sudo mkfs.xfs /dev/sdb 
meta-data=/dev/sdb               isize=512    agcount=4, agsize=655360 blks

$ sudo mount /dev/sdb /data

$ df /data
Filesystem     1K-blocks  Used Available Use% Mounted on
/dev/sdb        10475520 43472  10432048   1% /data

$ sudo mkdir /data/ubuntu

$ sudo chown ubuntu /data/ubuntu

$ echo "hello there" > /data/ubuntu/wow
```

# Questions?

Me every day at my desk: 

![picture](https://image.shutterstock.com/z/stock-photo-business-network-and-future-technology-concept-close-up-of-businessman-hand-pointing-finger-to-1039068475.jpg)





