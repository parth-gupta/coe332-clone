Homework 6
==========

**Q1.** Create a Dockerfile to containerize your Flask API. 

**Q2.** Build your Dockerfile and push it to DockerHub. This means that your image name will be similar to the following: `[your-dockerhub-username]/hw6`    
*Note: You may need to create a DockerHub account. This account is free.*

**Q3.** Using your newly built image, set up a docker-compose.yml file with your Flask API. You should be able to run your project using `docker-compose up`. 

***Be sure to include the following files for the assignment:***

- Your Dockerfile

- A README with information on your DockerHub image

- A docker-compose.yml file

Your other project files can be included if it�s easier for you, but not required. 
