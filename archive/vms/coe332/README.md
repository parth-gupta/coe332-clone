# scripts for coe332

## student access

- students must ssh from coe332.tacc.utexas.edu (isp02.tacc) because that is where their private ssh key lives 
- must ssh as user ubuntu, e.g. 
    
    ssh ubuntu@129.114.16.222

## setup 

- Have pubkeys created in students' home directories on coe332.tacc.utexas.edu
- Have students' vms created in openstack jetstream
- Have students' vms' ip addresses and pubkeys defined in coe332-spring2019-inventory.yml
- BONUS: You probably want to copy ssh-config to ~/.ssh/config here. It will prevent ssh/ansible asking for you to type yes a bunch of times the first time you connect to an address.

## ad hoc commands

- Be on coe332.tacc.utexas.edu
- Be jchuah, akahn, charlie, or mpackard

    ansible -i coe332-spring2019-inventory.yml all -a uptime 

## setup students vms 

- Put everything you want all the vms to have in student-vm-prep.yml
- (This is idempotent, does not hurt to run more than once)
- e.g. if you want to add more packages, put them in the "Install a list of packages" task and re-run

    ansible-playbook -i coe332-spring2019-inventory.yml student-vm-prep.yml
