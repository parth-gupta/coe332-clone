Final Project Description
-------------------------

**Description** - The final project will consist provide a REST API frontend to a time series database that also allows users to submit analysis jobs. At a minimum, the system should support an �analysis job� to create a plot of the data; extra credit can be given if the system supports additional types of analysis jobs, deploy on more than one VM, or has inventive user interfaces. 

The project will also include two separate pieces of documentation: the first will provide instructions for deploying the system to at least one VM and the second should be geared towards users/developers who will interact with the system.


**Required Components**

* Front-end APIs - A set of synchronous API endpoints providing the following functionality:

    * Data points retrieval endpoints
    * Endpoint for creating new data points.
    * Jobs/graph submission and retrieval endpoints.
    * **Submission of other kinds of analysis jobs (extra credit)**

* Back-end workers - Backend/worker processes to work the submitted jobs.
    * Worker processes framework.
    * Analysis job itself (e.g., make a graph) 
* Use of Redis database and queue structures to link front-end and back-end processes. 
* Note that if the API, Workers, and Redis server might run on a different servers, you�ll need to provide a configuration description (or better yet a way of propagating config) 
* Repository/code organization - The code should be organized into modules and directories that make it easy to navigate as the project grows. An example repository layout is included at https://bitbucket.org/tacc-cic/coe332/src/master/final_project_example/
* Documentation
    * Deployment docs - instructions for how an operator should deploy the system.
        * Instructions for deploying to one VM
        * If you are going for extra credit, provide instructions for deploying to two (or more) VMs, with any necessary instructions for configuring IP addresses for databases, etc.
    * User docs - instructions for how to interact with your API. This should more or less be a (possibly updated version of) your HW 5, e.g., 
        * List of endpoints
        * Expected JSON responses
        * Examples of how to use your system from within curl and Python.

#### Extra Credit

Extra credit can be received for implementing additional and/or more interesting analysis jobs, deploy on more than one VM, or has inventive user interfaces. Number of extra credit points is up to the discretion of the instructors. Use your imagination and creativity. Bring up questions/discussion in class.


#### Grading
Project represents 40% of the total class grade.

## PROJECT DUE MAY 17TH at 11:59 PM
