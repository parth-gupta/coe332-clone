## HW 2 parts A & B
Using your creature creator dataset, use your get_data() function that reads in your data set into a dictionary.

```
def get_data():
....
```
You job is to create an API to manage that database. We need to think through the following:

  * What are the nouns in our application?
  * What are the routes we want to define?
  * What data format do we want to return?

### Homework
#### Part A
  Create some new GET routes for the nouns identified in the database above.
  Find yout nouns, make at least 3 routes to retrieve the nouns from your json data
#### Part B
  Write tests for your routes
#### Part C
  Implement three query parameters and routes to query your data. (for querying dictionaries, see: https://developer.rhino3d.com/guides/rhinopython/python-dictionary-database/)
#### Part D
  Should go without saying, write unit tests for the new parameter routes

