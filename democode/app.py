from flask import Flask
from hotqueue import HotQueue
import json, redis, datetime, uuid


app = Flask(__name__)
rd = redis.StrictRedis(host='127.0.0.1', port=6379, db=7)


def get_data(datafilename):
	with open(datafilename) as json_file:
		userdata = json.load(json_file)
	return userdata

def new_job(item):
	##add item to database
	print(item)
	##add item to queue
	print(item)


mydata = get_data("data_file.json")
print (mydata)

@app.route('/', methods=['GET'])
def helloworld():
	print (mydata)
	return "Hello World"


@app.route('/head/<headtype>', methods=["GET"])
def getbyhead(headtype):
	worker_command = {"command":[]}
	uid = str(uuid.uuid4())
	command = "getbyhead"
	parameter = headtype	
	timestamp = str(datetime.datetime.now())
	item = {}
	item["uid"] = uid
	item["command"] = command 
	item["parameter"] = parameter
	item["timestamp"] = timestamp
	new_job(item)



	dataq = {"data":[]}
	for item in mydata['data']:
		if (item.get('head') == headtype):
			print (item)
			dataq["data"].append(item)
	return dataq
	
# the next statement should usually appear at the bottom of a flask app
if __name__ == '__main__':
   app.run(debug=True, host='0.0.0.0')




