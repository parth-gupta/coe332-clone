from flask import Flask
import json


app = Flask(__name__)

def get_data(datafilename):
	with open(datafilename) as json_file:
		userdata = json.load(json_file)
	return userdata



mydata = get_data("data_file.json")
print (mydata)

@app.route('/', methods=['GET'])
def helloworld():
	print (mydata)
	return "Hello World"


@app.route('/head/<headtype>', methods=["GET"])
def getbyhead(headtype):
	dataq = {"data":[]}
	for item in mydata['data']:
		if (item.get('head') == headtype):
			print (item)
			dataq["data"].append(item)
	return dataq
	
# the next statement should usually appear at the bottom of a flask app
if __name__ == '__main__':
   app.run(debug=True, host='0.0.0.0')

